﻿'testCaseURL:http://vlfcrailsp01/testrail/index.php?/cases/view/1573771



'Description:Verify user able to logout from request Xpress

'Pre-Condition  User should have valid USER_ID in the AD (Active Directory) group
'Created By                    :  Saurabh Verma
'===================================================================================================





'Step 1: Open IE browser andnavigate to RX

	openURL(ENVIRONMENT.Value("URL"))

	maximizeBrowser()



'Step 2 : Select MSR application from list display in All tab

	If 	Browser("mainBrowser").Exist(10) Then
		
		' Select MSR Application  html id:=tblStyle
		Browser("mainBrowser").Page("RequestXpressPage").Link("All").highlight
		Browser("mainBrowser").Page("RequestXpressPage").Link("All").Click
		
		
			Browser("mainBrowser").Page("RequestXpressPage").WebList("txtRequest1").highlight
			
			Browser("mainBrowser").Page("RequestXpressPage").WebList("txtRequest1").Select(0)
			Browser("mainBrowser").Page("RequestXpressPage").WebList("txtRequest1").Click
	
		
		

	End If
	
	'Step : Presence of logout 
	
	Browser("mainBrowser").Page("RequestXpressPage").Frame("Menu").Link("LOGOUT").highlight
			
	Browser("mainBrowser").Page("RequestXpressPage").Frame("Menu").Link("LOGOUT").Click

	
	



'Step 4: Close URL 

CloseAllBrowser()