﻿'testCaseURL:http://vlfcrailsp01/testrail/index.php?/cases/view/1573



'Description:Verify user able to select any application from Request Xpress application select page

'Pre-Condition  User should have valid USER_ID in the AD (Active Directory) group
'Created By                    :  Saurabh Verma
'===================================================================================================





'Step 1: Open IE browser andnavigate to RX

	openURL(ENVIRONMENT.Value("URL"))

	maximizeBrowser()



'Step 2 : Select MSR application from list display in All tab

	If 	Browser("mainBrowser").Exist(10) Then
		
		' Select MSR Application  html id:=tblStyle
		Browser("mainBrowser").Page("RequestXpressPage").Link("All").Click
		
		
		countOfItem = Browser("mainBrowser").Page("RequestXpressPage").WebList("txtRequest0").GetROProperty("items count")
		
		expectedString = uCase(trim("MSR - Manufacturing System Request"))  'Can be later Parameterize
		
		For Iterator = 1 To countOfItem Step 1
		
			item = Browser("mainBrowser").Page("RequestXpressPage").WebList("txtRequest0").GetItem(Iterator)
			item = ucase(trim(item))
			
								
		
			If (strcomp(item,expectedString)=0) Then
			
			
			
			Browser("mainBrowser").Page("RequestXpressPage").WebList("txtRequest0").Select(Iterator-1)
			Browser("mainBrowser").Page("RequestXpressPage").WebList("txtRequest0").Click
			
			Exit for 
				
			End If
			

			
			
		Next
		
		

	End If
	
	If Browser("mainBrowser").Page("RequestXpressPage").Frame("Menu").WebElement("Application:  MSR").Exist(20) then
			
			Browser("mainBrowser").CaptureBitmap "C:\RequestExpress\ResultImage\RX_002.bmp", True
	
			Reporter.ReportEvent micPass , "Application home page is seen" ,"Passed" ,"C:\RequestExpress\ResultImage\RX_002.bmp"
			
	else
	
			Browser("mainBrowser").CaptureBitmap "C:\RequestExpress\ResultImage\RX_002.bmp", True
	
			Reporter.ReportEvent micPass , "Application home page is NOT seen" ,"Failed" ,"C:\RequestExpress\ResultImage\RX_002.bmp"
	
	End IF
	
	



'Step 4: Close URL 

CloseAllBrowser()


