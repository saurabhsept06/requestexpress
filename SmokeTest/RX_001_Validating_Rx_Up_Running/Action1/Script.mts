﻿'testCaseURL:http://vlfcrailsp01/testrail/index.php?/cases/view/1573766

'Description:Verify user able to access Request Xpress

'Pre-Condition  User should have valid USER_ID in the AD (Active Directory) group
'Created By                    :  Saurabh Verma
'===================================================================================================





'Step 1: Open IE browser andnavigate to RX

	openURL(ENVIRONMENT.Value("URL"))

	maximizeBrowser()



'Step 2 : Rx application should open 

	If Browser("title:=RequestXpress").Exist(10) Then
		
		Reporter.ReportEvent micPass, "The Request Xpress page is loaded" , "Passed"
		
	End If
	


'Step 4: Close URL 

CloseAllBrowser()


