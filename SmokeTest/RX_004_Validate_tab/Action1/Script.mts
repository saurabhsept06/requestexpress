﻿'testCaseURL:http://vlfcrailsp01/testrail/index.php?/cases/view/1573769



'Description:Verify user able to change from one tab to another tab in application selection page

'Pre-Condition  User should have valid USER_ID in the AD (Active Directory) group
'Created By                    :  Saurabh Verma
'===================================================================================================





'Step 1: Open IE browser andnavigate to RX

		openURL(ENVIRONMENT.Value("URL"))

	maximizeBrowser()



'Step 2 : Select MSR application from list display in All tab

	If 	Browser("mainBrowser").Exist(10) Then
		
		' Select MSR Application  html id:=tblStyle
		Browser("mainBrowser").Page("RequestXpressPage").Link("All").highlight
		Browser("mainBrowser").Page("RequestXpressPage").Link("All").Click
		
		

	End If
	
	
	
	If 	Browser("mainBrowser").Page("RequestXpressPage").Link("FAB 10").Exist (10) then 
	
		Browser("mainBrowser").Page("RequestXpressPage").Link("FAB 10").highlight
		Browser("mainBrowser").Page("RequestXpressPage").Link("FAB 10").Click
	
			Browser("mainBrowser").CaptureBitmap "C:\RequestExpress\ResultImage\RX_004.bmp", True
	
			Reporter.ReportEvent micPass , "Tab is seen and clicked" ,"Passed" ,"C:\RequestExpress\ResultImage\RX_004.bmp"
			
	else
	
			Browser("mainBrowser").CaptureBitmap "C:\RequestExpress\ResultImage\RX_002.bmp", True
	
			Reporter.ReportEvent micPass , "Tab is not seen or clicked" ,"Failed" ,"C:\RequestExpress\ResultImage\RX_004.bmp"
	
	End If




'Step 4: Close URL 

CloseAllBrowser()

