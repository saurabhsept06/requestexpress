﻿'testCaseURL:http://vlfcrailsp01/testrail/index.php?/cases/view/1573769



'Description:Verify user able to change from one tab to another tab in application selection page

'Pre-Condition  User should have valid USER_ID in the AD (Active Directory) group
'Created By                    :  Saurabh Verma
'===================================================================================================





'Step 1: Open IE browser andnavigate to RX


	openURL(ENVIRONMENT.Value("URL"))

	maximizeBrowser()

 acutalResult =  Browser("mainBrowser").Page("RequestXpressPage").WebTable("selectionTable").GetCellData(1,1)

 expectResult = Browser("mainBrowser").Page("RequestXpressPage").WebTable("UserName").GetROProperty("text")

'Step 2 : Select MSR application from list display in All tab

	If 	Browser("mainBrowser").Exist(10) Then
		
		customList acutalResult,expectResult
		Browser("mainBrowser").Page("RequestXpressPage").Link("lnkALL").highlight
		
		

	End If
	
'Step 3: Click on "Customize my list of Application" link

Browser("mainBrowser").Page("RequestXpressPage").Link("lnkCustomizeList").highlight 
Browser("mainBrowser").Page("RequestXpressPage").Link("lnkCustomizeList").Click


'Step 4: Select list of applications from "Applications not in my list" box and click on "&gt;&gt;" button

If Browser("mainBrowser").Page("RequestXpressPage").WebList("listCustom").Exist(10) Then

	Browser("mainBrowser").Page("RequestXpressPage").WebList("listCustom").Select(1)
	
	Browser("mainBrowser").Page("RequestXpressPage").WebButton("addButton").highlight
	Browser("mainBrowser").Page("RequestXpressPage").WebButton("addButton").Click
	
	
	'Step 5: Click on Save button and Back button
	
	Browser("mainBrowser").Page("Page").WebButton("Save").highlight
	Browser("mainBrowser").Page("Page").WebButton("Save").Click
	Browser("mainBrowser").Page("Page").WebButton("Back").Click
	Browser("mainBrowser").Page("RequestXpressPage").Sync


End If

'Step 6: Customized list of application User tab should be created
 
 
 
 If (strComp(extractWord(acutalResult,0),extractWord(expectResult,1),1)) = 0 Then
 	
 			Browser("mainBrowser").CaptureBitmap "C:\RequestExpress\ResultImage\RX_005.bmp", True
		
			Reporter.ReportEvent micPass , "Application home page is seen" ,"Passed" ,"C:\RequestExpress\ResultImage\RX_005.bmp"
			
	else
	
			Browser("mainBrowser").CaptureBitmap "C:\RequestExpress\ResultImage\RX_005.bmp", True
	
			Reporter.ReportEvent micPass , "Application home page is NOT seen" ,"Failed" ,"C:\RequestExpress\ResultImage\RX_005.bmp"
 End If



'Step 7: Tear Down



	customList acutalResult ,expectResult


CloseAllBrowser()





'================================

Function customList(acutalResult ,expectResult)
	str1 = cstr(acutalResult)
	 str2 = cstr(extractWord(expectResult,1))
	 'msgbox (instr(str1,str2))

	If  (instr(str1,str2)) > 0 Then
	
		Browser("mainBrowser").Page("RequestXpressPage").Link("lnkCustomizeList").highlight 
		Browser("mainBrowser").Page("RequestXpressPage").Link("lnkCustomizeList").Click
		Browser("mainBrowser").Page("Page").Sync
		Browser("mainBrowser").Page("Page").WebList("selInclude").Select(0)
		Browser("mainBrowser").Page("Page").WebButton("Remove").Click
		Browser("mainBrowser").Page("Page").WebButton("Save").highlight
		Browser("mainBrowser").Page("Page").WebButton("Save").Click
		Browser("mainBrowser").Page("Page").WebButton("Back").Click
		Browser("mainBrowser").Page("RequestXpressPage").Sync
		
		
	End If
	
End Function
