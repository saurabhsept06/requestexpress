﻿'testCaseURL:http://vlfcrailsp01/testrail/index.php?/cases/view/1573768



'Description:Verify user able to re-navigate to application selection page

'Pre-Condition  User should have valid USER_ID in the AD (Active Directory) group
'Created By                    :  Saurabh Verma
'===================================================================================================





'Step 1: Open IE browser andnavigate to RX

	openURL(ENVIRONMENT.Value("URL"))

	maximizeBrowser()



'Step 2 : Select MSR application from list display in All tab

	If 	Browser("mainBrowser").Exist(10) Then
		
		' Select MSR Application  html id:=tblStyle
		Browser("mainBrowser").Page("RequestXpressPage").Link("All").Click
		
		
		countOfItem = Browser("mainBrowser").Page("RequestXpressPage").WebList("txtRequest0").GetROProperty("items count")
		
		expectedString = uCase(trim("MSR - Manufacturing System Request"))  'Can be later Parameterize
		
		For Iterator = 1 To countOfItem Step 1
		
			item = Browser("mainBrowser").Page("RequestXpressPage").WebList("txtRequest0").GetItem(Iterator)
			item = ucase(trim(item))
			
								
		
			If (strcomp(item,expectedString)=0) Then
			
			
			
			Browser("mainBrowser").Page("RequestXpressPage").WebList("txtRequest0").Select(Iterator-1)
			Browser("mainBrowser").Page("RequestXpressPage").WebList("txtRequest0").Click
			
			Exit for 
				
			End If
			

			
			
		Next
		
		

	End If
	
	
	' Step 3: Click on Change Application link on top right corner in the page
	
	If Browser("mainBrowser").Page("RequestXpressPage").Frame("Menu").WebElement("Application:  MSR").Exist(20) then
			
		Browser("mainBrowser").Page("RequestXpress").Frame("Menu").Link("CHANGE APPLICATION").highlight	
		Browser("mainBrowser").Page("RequestXpress").Frame("Menu").Link("CHANGE APPLICATION").Click

	End IF
	
	
	'Step 4: User navigate to Main page 
	
		If Browser("mainBrowser").Page("RequestXpressPage").Link("All").Exist(20) Then
		
	
			Browser("mainBrowser").CaptureBitmap "C:\RequestExpress\ResultImage\RX_003.bmp", True
	
			Reporter.ReportEvent micPass , "User navigate back to application selection page" ,"Passed" ,"C:\RequestExpress\ResultImage\RX_003.bmp"
			
			else
	
			Browser("mainBrowser").CaptureBitmap "C:\RequestExpress\ResultImage\RX_002.bmp", True
	
			Reporter.ReportEvent micPass , "User is not navigate back to application selection page"  ,"Failed" ,"C:\RequestExpress\ResultImage\RX_003.bmp"
		
		End If


'Step 4: Close URL 

CloseAllBrowser()


